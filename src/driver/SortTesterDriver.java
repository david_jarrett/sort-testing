package driver;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import model.SortTester;
import utility.SortTesterReporter;
import utility.SortTools;

/**
 * This is the driver class for the application. It creates a SortTester object for each
 * sort method in the SortTools class, and then runs each one in turn. When finished,
 * it generates a report and saves it to FILE_NAME. 
 * @author  David Jarrett
 * @version 2/2/2018
 *
 */
public class SortTesterDriver {
    
    private static final String FILE_NAME = "/report.csv";

    public static void main(String[] args) {
        List<SortTester> sortTesters = getSortTesters();
        System.out.println("Begin...");
        
        try {
            for (SortTester currentTester : sortTesters) {
                currentTester.beginTesting();
                System.out.println(currentTester.getSortTitle() + ": done");
            }
            createReport(sortTesters);
            System.out.println("done");
        } catch (InterruptedException e) {
            System.err.println("Something interrupted a thread. Aborting...");
        }
    }

    private static List<SortTester> getSortTesters() {
        List<SortTester> sortTesters = new ArrayList<>();
        sortTesters.add(new SortTester("Insertion Sort", SortTools::insertionSort));
        sortTesters.add(new SortTester("Bubble Sort", SortTools::bubbleSort));
        sortTesters.add(new SortTester("Selection Sort", SortTools::selectionSort));
        sortTesters.add(new SortTester("Merge Sort", SortTools::mergeSort));
        sortTesters.add(new SortTester("Quick Sort", SortTools::quickSort));
        
        return sortTesters;
    }
    
    private static void createReport(List<SortTester> sortTesters) {
        try {
            String outputFilename = System.getProperty("user.dir") + FILE_NAME;
            PrintWriter fileOutput = new PrintWriter(outputFilename, "UTF-8");
            SortTesterReporter reporter = new SortTesterReporter(sortTesters, fileOutput);
            reporter.writeReport();
            fileOutput.close();
        } catch (FileNotFoundException e) {
            System.err.println("The output file could not be generated.");
        } catch (UnsupportedEncodingException e) {
            System.err.println(e.getMessage());
        }
    }

}
