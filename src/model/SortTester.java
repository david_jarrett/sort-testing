package model;

import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

import utility.SortTools;

/**
 * This class will run a user-supplied sorting algorithm on arrays of Integers. The arrays
 * range in size from MIN_ELEMENT_COUNT to MAX_ELEMENT_COUNT. The algorithm will be executed ITERATION_COUNT
 * times, and an average execution time will be calculated. Each successive array is
 * STEP_SIZE elements larger than the preceding one. Each array is shuffled prior to
 * each execution of the sorting algorithm.<br><br>
 * 
 * A single complete execution of the sorting algorithm on a given array 
 * is carried out its own thread. This class concurrently runs a number of
 * threads equal to 1.5X the number of CPUs available on the computer. The result
 * of running each SortIteration (thread) is stored in this object.
 * 
 * @author  David Jarrett
 * @version 2/2/2018
 *
 */
public class SortTester {
    
    public static final int MIN_ELEMENT_COUNT = 1_000;
    public static final int MAX_ELEMENT_COUNT = 100_000;
    public static final int STEP_SIZE = 1_000;
    public static final int ITERATION_COUNT = 20;
    public static final int VALUE_NOT_FOUND = -1;
    
    private String sortTitle;
    
    private Queue<Thread> threadQueue;
    private int nextArraySize;
    
    private Consumer<Integer[]> sortMethod;
    private Map<Integer, Long> results;
        
    /**
     * Initializes the class.
     * <p>
     * <strong>precondition :</strong> sortTitle != null && sortMethod != null<br><br>
     * <strong>postcondition:</strong> The object will be initialized and ready to begin testing.
     * </p>
     * 
     * @param sortTitle The name of the sorting algorithm being tested.
     * @param sortMethod A method reference to the sorting algorithm.
     */
    public SortTester(String sortTitle, Consumer<Integer[]> sortMethod) {
        this.sortMethod = Objects.requireNonNull(sortMethod, "The supplied sort method cannot be null.");
        this.sortTitle = Objects.requireNonNull(sortTitle, "The supplied title cannot be null.");
        this.results = new Hashtable<>();
        this.threadQueue = new ConcurrentLinkedQueue<>();
        this.nextArraySize = MIN_ELEMENT_COUNT;
    }
    
    /**
     * This method starts the testing process.
     * @precondition: None
     * @postcondition: The tests will be complete and the results will be available.
     * @throws InterruptedException - Throw if a thread is interrupted. Not used.
     */
    public void beginTesting() throws InterruptedException {
        this.loadThreadQueue();
        this.cycleThreads();
    }

    private void loadThreadQueue() {
        int cpuCount = (int) (Runtime.getRuntime().availableProcessors() * 1.5);
        for (int cpusLeft = cpuCount; cpusLeft > 0 && this.nextArraySize <= MAX_ELEMENT_COUNT; cpusLeft--) {
            Thread iteration = new SortIteration(this.nextArraySize);
            iteration.start();
            
            this.nextArraySize += STEP_SIZE;
            this.threadQueue.add(iteration);
        }
    }
    
    private void cycleThreads() throws InterruptedException {
        while (!this.threadQueue.isEmpty()) {
            Thread youngestThread = this.threadQueue.poll();
            youngestThread.join();
            
            this.replaceFinishedThreadInQueue();
        }
    }
    
    private void replaceFinishedThreadInQueue() {
        if (this.nextArraySize <= MAX_ELEMENT_COUNT) {
            Thread replacement = new SortIteration(this.nextArraySize);
            replacement.start();
            this.threadQueue.add(replacement);
            this.nextArraySize += STEP_SIZE;
        }
    }

    /**
     * Returns the title of the sorting algorithm that this object is testing.
     * @precondition: None
     * @return The title.
     */
    public String getSortTitle() {
        return this.sortTitle;
    }
    
    /**
     * Returns the time it took for the array with elementCount elements to be sorted.
     * @precondition: None
     * 
     * @param elementCount - The number of elements in the array that you're seeking.
     * @return The time it took to sort the array with elementCount elements, or
     *         SortTester.VALUE_NOT_FOUND if the result doesn't exist.
     */
    public long getSortTimeFor(int elementCount) {
        if (this.results.containsKey(elementCount)) {
            return this.results.get(elementCount);
        }
        return VALUE_NOT_FOUND;
    }
    
    /**
     * This class represents a run of the sorting algorithm. It executes the algorithm
     * SortTester.ITERATION_COUNT times on an array of size elementCount.    
     * @author  David Jarrett
     * @version 2/2/2018
     */
    public class SortIteration extends Thread {

        private long cumulativeRunTime;
        private int elementCount;
        
        /**
         * Initializes the object.
         * @precondition: elementCount >= 0
         * @postcondition: The iteration will be ready to start.
         * 
         * @param elementCount - The size of the array to be sorted within this iteration.
         */
        public SortIteration(int elementCount) {
            if (elementCount < 0) {
                throw new IllegalArgumentException("The element count must be nonnegative.");
            }
            this.elementCount = elementCount;
            this.cumulativeRunTime = 0L;
        }
        
        /**
         * Starts this iteration, sorting an array SortTester.ITERATION_COUNT times and
         * storing the resulting average execution time in SortTester.
         * @precondition: None
         * @postcondition: The iteration will be complete, with the results stored in SortTester.
         */
        @Override
        public void run() {
            Integer[] array = this.getFilledArray();
            
            for (int i = 0; i < SortTester.ITERATION_COUNT; i++) {
                SortTools.shuffle(array);
                
                long startTime = System.currentTimeMillis();
                SortTester.this.sortMethod.accept(array);
                long endTime = System.currentTimeMillis();
                
                this.cumulativeRunTime += endTime - startTime;
            }
            
            long averageTime = cumulativeRunTime / SortTester.ITERATION_COUNT;
            SortTester.this.results.put(this.elementCount, averageTime);
        }

        private Integer[] getFilledArray() {
            Integer[] array = new Integer[this.elementCount];
            for (int i = 0; i < this.elementCount; i++) {
                array[i] = i;
            }
            return array;
        }
    }
    
}
