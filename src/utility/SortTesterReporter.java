package utility;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Objects;

import model.SortTester;

/**
 * This class generates a report utilizing information stored in a list of SortTester objects.
 * @author  David Jarrett
 * @version 2/2/2018
 *
 */
public class SortTesterReporter {
    
    private static final String NEWLINE = System.lineSeparator();
    
    private List<SortTester> sortTesters;
    private Writer out;
    
    /**
     * Initializes the class.
     * @precondition: sortTesters != null && out != null
     * @postcondition: Object will be initialized and ready to report.
     * 
     * @param sortTesters - The SortTester objects to report on.
     * @param out - The destination of all report output.
     */
    public SortTesterReporter(List<SortTester> sortTesters, Writer out) {
        this.sortTesters = Objects.requireNonNull(sortTesters, "List of SortTesters cannot be null.");
        this.out = Objects.requireNonNull(out, "The output object cannot be null.");
    }
    
    /**
     * Pushes the entire report to the output destination.
     * @precondition: None
     * @postcondition: The report will be sent to the output destination.
     */
    public void writeReport() {
        try {
            this.writeOutHeader();
            
            for (int currentElement = SortTester.MIN_ELEMENT_COUNT; currentElement <= SortTester.MAX_ELEMENT_COUNT;
                    currentElement += SortTester.STEP_SIZE) {
                 this.writeOutRow(currentElement);
            }
 
            this.out.write(NEWLINE);
            this.out.flush();
        } catch (IOException e) {
            System.err.println("There was an IO error when writing the report.");
        }
    }

    private void writeOutHeader() throws IOException {
        this.out.write("n,");
        for (int i = 0; i < sortTesters.size(); i++) {
            this.out.write(sortTesters.get(i).getSortTitle() + ",");
        }
        out.write("n^2,nlog(n)");
    }
    
    private void writeOutRow(int currentElement) throws IOException {
        this.out.write(NEWLINE + currentElement + ",");
        for (SortTester currentTester : this.sortTesters) {
            long averageTime = currentTester.getSortTimeFor(currentElement);
            if (averageTime != SortTester.VALUE_NOT_FOUND) {
                out.write(averageTime + ",");
            } else {
                reportError(currentElement, currentTester);
            }
        }
        double nSquared = (double)currentElement * currentElement;
        double nlogn = currentElement * Math.log(currentElement);
        out.write(nSquared + "," + nlogn);
    }

    private void reportError(int currentElement, SortTester currentTester) {
        System.err.println(currentTester.getSortTitle() + " does not have an entry for a " + currentElement
                + " element array. Writing nothing for this entry.");        
    }

}
